### README

## Author: Adam Brewer , abrewer2@uoregon.edu

## Description: A simple web server that serves html & css pages, found in the "pages" directory, to an active server.

## Assignment
* Fork this repository to create your own repository on Bitbucket.  
* Clone your repository onto the machine you want to work on.  
* Add the following functionality in pageserver.py.   
(a) If URL ends with `name.html` or `name.css` (i.e., if `path/to/name.html` is in document path (from DOCROOT)), send content of `name.html` or `name.css` with proper http response.
(b) If `name.html` is not in current directory Respond with 404 (not found).  
(c) If a page starts with one of the symbols(~ // ..), respond with 403 forbidden error. For example, `url=localhost:5000/..name.html` or `/~name.html` would give 403 forbidden error.
* Commit and push ALL your changes to github (except those not under revision control)
* Test deployment in other environments.
